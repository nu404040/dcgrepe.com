# WSEALUG's homepage

## Development

The site uses jekyll, a static site generator. Learn more
about [jekyll here.](http://jekyllrb.com/)

### Contributing

Smaller edits to the site can be made with git, an editor and a gitlab.com account:

1. Fork the repo, https://gitlab.com/wsealug/wsealug.net, to your gitlab account.
2. Clone the fork from your gitlab account to your local machine:   
```
    $ git clone git@gitlab.com:<gitlabId>/wsealug.net.git
```
3. Create and check out a branch, ``<testbranch>``, for your changes.
```
    $ git checkout -b <testbranch>
```
4. Make your changes and commit them locally.
```
    $ git add -u 
    $ git commit -m 'Making things better!'
```
5. Push your changes back to your gitlab account. Gitlab will build and publish a test site for review.
```
    $ git push origin <testbranch>
```
6. Monitor the build using the Gitlab project ``CI/CD|Pipelines`` page.
7. Browse to the test site at ``https://<gitlabId>.gitlab.io/wsealug.net`` to review your changes.
8. Create a merge request to merge the ``<testbranch>`` changes back to https://gitlab.com/wsealug/wsealug.net
9. (optionally) Delete the test site using the Gitlab project ``Settings|Pages`` page.

To test or debug changes on your local machine, run a jekyll server as described below.

### Prerequisites

1. Ruby 2.5.5
   - Check your system by typing in `ruby -v`
   - you may have to install [rvm](http://rvm.io) to get it running

2. bundler `gem install bundler`

### Quick set up

#### Without a container

    $ git clone git@gitlab.com:wsealug/wsealug.net.git
    $ cd wsealug.net
    $ bundle install
    $ bundle exec jekyll serve

Now browse to http://localhost:4000

#### Using Docker

    $ git clone git@gitlab.com:wsealug/wsealug.net.git
    $ cd wsealug.net
    $ docker build -t wsealug-website .
    $ docker run --rm -it -p 127.0.0.1:4000:4000 --mount type=bind,src=$(pwd),dst=/usr/src/app wsealug-website

Now browse to http://localhost:4000

### Adding a blog entry
- create a new file in the `_posts`  directory using the format YYYY-MM-DD-title.md (see other files in directory for examples of the header)
- Write the entry in [markdown](https://daringfireball.net/projects/markdown/syntax)
- If you're developing using `jekyll serve`, your post should be available locally immediately. If using gh-pages, it should be live once the changes are pushed to gh-pages branch on github


### Development Resources

- [jekyll](http://jekyllrb.com/)
- [markdown](https://daringfireball.net/projects/markdown/syntax)
- [liquid templating](https://shopify.github.io/liquid/)  
- [bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)


### WSEALUG Resources
- [gitlab](https://gitlab.com/wsealug)
- [gettogether](https://gettogether.community/wsealug/)
- [twitter](https://twitter.com/WSeaLUG)
- [email](contact@wsealug.net)
- [slack signup](https://wsealug-slack-signup.herokuapp.com/)
- [Matrix #wsealug:seattlematrix.org](https://matrix.to/#/#wsealug:seattlematrix.org)


### TODO
- Move social media links under logo
- Finish Pages
  - News
  - Events
  - Past Presentations
  - About
- Make the mascot svg
- adjust styling and get opinion on design
- discuss content (adding/removing/copy fixes) with other members
- sneak in a seaslug on the site

### LATER

- Linux Resources page
- Bio for organizers
